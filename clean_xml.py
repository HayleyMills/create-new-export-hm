#!/usr/bin/env python3

"""
Python 3

    Clean xml

    grep -rnw 'archivist' -e '&amp;amp;'
    archivist/alspac_91_pq.xml:16376:          <r:Content xml:lang="en-GB">City &amp;amp; Guilds intermediate technical</r:Content>
    archivist/alspac_91_pq.xml:16385:          <r:Content xml:lang="en-GB">City &amp;amp; Guilds final technical</r:Content>
    archivist/alspac_91_pq.xml:16394:          <r:Content xml:lang="en-GB">City &amp;amp; Guilds full technical</r:Content>
    archivist/alspac_91_pq.xml:16412:          <r:Content xml:lang="en-GB">Yes &amp;amp; affected me a lot</r:Content>

"""

from lxml import etree
import pandas as pd
import time
import sys
import os
import re


def getDirectoryList(path):
    """
    return a list of all directories which contain at least one .xml file.
    """
    directoryList = []

    #return nothing if path is a file
    if os.path.isfile(path):
        return []

    #add dir to directorylist if it contains .txt files
    if os.path.isdir(path):
        if len([f for f in os.listdir(path) if f.endswith('.xml')])>0:
            directoryList.append(path)

        for d in os.listdir(path):
            new_path = os.path.join(path, d)
            if os.path.isdir(new_path):
                directoryList += getDirectoryList(new_path)

    return directoryList


def clean_text(rootdir, outdir):
    """
    Go through text files in rootdir
        - replace &amp;amp;# with &#
        - replace &amp;amp; with &amp;
        - replace &amp;# with &#
        - replace &#160: with &#160;
        - replace &#163< with &#163;<
        - replace &amp;amp;amp;# with &#
    """

    try:
        files = [os.path.basename(f) for f in os.listdir(rootdir) if os.path.isfile(os.path.join(rootdir, f))]
    except WindowsError:
        print("something is wrong")
        sys.exit(1)

    for filename in files:
        filename = os.path.join(rootdir, filename)
        print(filename + r": pass 1 fixing repeat amps: '&amp;amp;...amp;#xx;' -> '&#xx;")

        tmpfile1 = filename + ".temp1"
        tmpfile2 = filename + ".temp2"
        tmpfile3 = filename + ".temp3"

        with open(filename, "r") as fin:
            with open(tmpfile1, "w") as fout:
                for line in fin:
                    # regex: at least one 'amp;' in \1 and digits in \2
                    fout.write(re.sub(r'\&(amp;)+#(\d+);', r'&#\2;', line))

        print(filename + ": pass 2 fixing '&amp;amp;'")
        with open(tmpfile1, "r") as fin:
            with open(tmpfile2, "w") as fout:
                for line in fin:
                    fout.write(line.replace("&amp;amp;", "&amp;"))

        print(filename + ": pass 3 fixing '&#160:' (note colon)")
        with open(tmpfile2, "r") as fin:
            with open(tmpfile3, "w") as fout:
                for line in fin:
                    fout.write(re.sub(r"(&#[0-9]+):", r"\1;", line))

        print(filename + r": pass 4 fixing '&#163<' (note < tag start")
        with open(tmpfile3, "r") as fin:
            # note overwrite the original file
            with open(os.path.join(outdir, os.path.basename(filename)), "w") as fout:
                for line in fin:
                    fout.write(line.replace("&#163<", "&#163;<"))

        # remove tmp
        print(filename + ": deleting tmpfile")
        os.unlink(tmpfile1)
        os.unlink(tmpfile2)
        os.unlink(tmpfile3)


def clean_newline(rootdir):
    """
    Overwrite the original xml file with a middle of context line break replaced by a space.
    Also expand the escaped characters, for example: &#163; becomes £
    Line breaks in text are generally represented as:
        \r\n - on a windows computer
        \r   - on an Apple computer
        \n   - on Linux
    """

    try:
        files = [f for f in os.listdir(rootdir) if os.path.isfile(os.path.join(rootdir, f))]
    except WindowsError:
        print("something is wrong")
        sys.exit(1)

    for filename in files:
        filename = os.path.join(rootdir, filename)
        print(filename)
        p = etree.XMLParser(resolve_entities=True)
        with open(filename, "rt") as f:
            tree = etree.parse(f, p)

        for node in tree.iter():
            if node.text is not None:
                if re.search("\n|\r|\r\n", node.text.rstrip()):
                    node.text = node.text.replace("\r\n", " ")
                    node.text = node.text.replace("\r", " ")
                    node.text = node.text.replace("\n", " ")

        # because encoding="UTF-8" in below options, the output can contain non-ascii characters, e.g. £
        tree.write(filename, encoding="UTF-8", xml_declaration=True)


def rm_duplicate_n(rootdir):
    """
    Remove text <duplicate [n]> from sequence labels in the XML after it has exported
    """

    try:
        files = [f for f in os.listdir(rootdir) if os.path.isfile(os.path.join(rootdir, f))]
    except WindowsError:
        print("something is wrong")
        sys.exit(1)

    for filename in files:
        # get file name and extension
        (only_name, only_extension) = os.path.splitext(filename)

        # adding the new name with extension
        new_base = only_name + '_rm_duplicate_n' + only_extension
        new_filename = os.path.join(rootdir, new_base)

        p = etree.XMLParser(resolve_entities=True)
        with open(os.path.join(rootdir, filename), "rt") as f:
            tree = etree.parse(f, p)

        check = []
        for node in tree.iter():
            if node.text is not None:
                check_duplicate = bool(re.search(r" duplicate \d+$", node.text))
                check.append(check_duplicate)
                node.text = re.sub(r" duplicate \d+$", "", node.text)

        if any(check) is True:
            tree.write(new_filename, encoding="UTF-8", xml_declaration=True)



def clean_xml(input_dir, output_dir):
    """
    Clean downloaded xml files
    """
    print("clean text")
    clean_text(input_dir, output_dir)
    print("clean new line")
    clean_newline(output_dir)
    print("rm duplicate n from sequence label")
    rm_duplicate_n(output_dir)


def main():

    # clean xml
    out_file = 'export_xml'
    xml_dir_list = getDirectoryList(out_file)

    for xml_dir in xml_dir_list:
        clean_xml_dir = xml_dir + '_clean'
        if not os.path.exists(clean_xml_dir):
            os.makedirs(clean_xml_dir)
            clean_xml(xml_dir, clean_xml_dir)


if __name__ == "__main__":
    main()


